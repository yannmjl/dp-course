# Security -- Gitlab Enterprise security scanning
> DevSecOps using GitLab Secure

## Objectives
This lab is the conintuty of Security, and Governance, Risk, and Compliance. You will gain some hands on experience in integretting Secruity pratcices such as vulnerability scanning and remediation within the Developement and operation. Thus, moving from DevOps CI/CD to DevSecOps CI/CD pipeline

## Overview
GitLab Secure provides Static Application Security Testing (SAST), Dynamic Application Security Testing (DAST), Container Scanning, and Dependency Scanning to help you deliver secure applications along with License Compliance.

For More information, check out the documenetation on https://docs.gitlab.com/ee/user/application_security/[GitLab secure].

## Getting Started
All the described securities Scan can be added to your DevOps CI/CD pipeline by includding the default templates provided by GitLab. Take a moment to review the code and get a sense of the templates do, https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates/Security[Security Templates]

The Security Configuration page displays what security scans are available, links to documentation and also simple enablement tools for the current project.

TIP: To view a project’s security configuration, go to the project’s home page, then in the left sidebar go to Security & Compliance > Configuration.

image::security_compliance.gif[]

### Enabling security Scans

You can configure the following security controls:

* Auto DevOps
NOTE: Enabling Auto DevOps will enable all the scana in your current project CI/CD pipeline. For more details, see https://docs.gitlab.com/ee/topics/autodevops/index.html[Auto DevOps].

* SAST
Click either Enable or Configure to use SAST for the current project. For more details, see https://docs.gitlab.com/ee/user/application_security/sast/index.html#configure-sast-in-the-ui[Configure SAST in the UI].
* DAST Profiles
Click Manage to manage the available DAST profiles used for on-demand scans. For more details, see https://docs.gitlab.com/ee/user/application_security/dast/index.html#on-demand-scans[DAST on-demand scans].

#### Enables SATS

Go to the project’s home page, then in the left sidebar go to Security & Compliance > Configuration.
Then Select Congiure Secuiryt Scan. Leave all the value to default and create a merge request to modify and include your the SAST template to your .gitlab-ci.yml file.

image::enable_SAST.gif[]

This can also be done manually by adding the following to our 
....
sast:
  stage: sastscan
include:
- template: Security/SAST.gitlab-ci.yml
....

NOTE: Make sure that the jobs in your .gitlab-ci.yml file are arranged in the order you want them to run. 
....
stages:
- npmtest
- sastscan
- dockerbuild
- gkedeploy
....

Commit the changes and whatch your new DevSecOps pipeline in action:

image::scan_SAST.gif[]

IMPORTANT: Submit a screen shot of your SAST job succufully run and output of your security Dashboard

#### Enable Container Scanning:

* This is done by Manually include the container template in your existing .gitlab-ci.yml file:
....
include:
- template: Container-Scanning.gitlab-ci.yml
....

The container scanning template run by test through the test stage on GitLAb CI/CD.
Thus, simply add the test stage in your job order after the docker build as showm below:

....
stages:
- npmtest
- sastscan
- dockerbuild
- test 
- gkedeploy
....

One of the requirement to enable container scanning in your pipeline is making sure that 
The name of the Docker image should use the following predefined CI/CD variables:
....
$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
....

* To acheive add or modified your docker build job accordigly. Which will look like this:
....
# build image that follows the predefined CI/CD variables rcongied by continaer scanning template
build:
  stage: dockerbuild
  image: docker:stable
  services:
    - docker:dind
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  before_script:
    - docker info
    - docker login registry.gitlab.com -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD}
  script:
    - docker build -t $IMAGE .
    - docker push $IMAGE
  after_script:
    - docker logout ${CI_REGISTRY}
....

* Now let's assure that the container scanning template is checking the correct image name
by overriding some variables of the contianer scanning template as follow:
....
# Overriding the container scanning template
container_scanning:
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
    # disable docker-in-docker for dependency scanning
    DS_DISABLE_DIND: "true"
  artifacts:
    paths: [gl-container-scanning-report.json]
....

Commit the changes and whatch your new DevSecOps pipeline in action:

image::container_scanning.gif[]

IMPORTANT: Submit a screen shot of your Container Saccner job succufully run and output of your security Dashboard

#### Enable dependency Scanning:

* This is done manually by include the its template in your existing .gitlab-ci.yml file:
....
include:
- template: Dependency-Scanning.gitlab-ci.yml
....

Dependency scanning will run under the test stage on GitLAb CI/CD.

image::dependency_scanning.gif[]

IMPORTANT: Submit a screen shot of your dependency scanner job succufully run and output of your Dependency List

#### Enable Secret-Detection:

* This is done manually by include the its template in your existing .gitlab-ci.yml file:
....
include:
- template: Security/Secret-Detection.gitlab-ci.yml
....

Dependency scanning will run under the test stage on GitLAb CI/CD.
Commit a change  and whatch your new DevSecOps pipeline in action and check your security and compliance board:

image::secret_scanning.gif[]

You have probalably noticed that we have many vulnerabilities due to outdated version of dependencies.

Now, Let's update the node js depenincy and other docker container while building the image.
To do so let's modify the dockerFileby adding the following:

* on your Dockerfile, removed the image version on node:11 so that the last image will be pulled from dockerHub:
....
# removed the version on node:11 based image
FROM node
....

* update node js app and docker container dependencies by adding the following in the Dockerfile: 
....
# update dependencies and install curl
RUN apt-get update && apt-get install -y \
    curl \
    && rm -rf /var/lib/apt/lists/*
....
....
# update each dependency in package.json to the latest version
RUN npm install -g npm-check-updates
RUN ncu -u
RUN npm install
RUN npm install express
....

Run your piple and compare to see if which Vulnerabilities have been resolved due to the update: 

image::fixed_vulnerability.gif[]
